package com.project.myacademy.domain.uniqueness.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UpdateUniquenessRequest {
    private String body;
}
